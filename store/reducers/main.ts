import { createReducer } from "@reduxjs/toolkit";

const initialState = {};

export const mainReducer = createReducer(initialState, (builder) => {
  builder.addDefaultCase((state) => state);
});
