import { createAction } from "@reduxjs/toolkit";

export enum MainActions {
  DUMMY_ACTION = "DUMMY_ACTION",
}

export const dummyAction = createAction<{ data: any }>(
  MainActions.DUMMY_ACTION
);
