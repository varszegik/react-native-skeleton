import MainScreen from "../screens/MainScreen";

import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";

export enum Routes {
  MAIN = "Main",
}

const MainNavigator = createStackNavigator({
  //@ts-ignore https://github.com/react-navigation/react-navigation/issues/6516
  [Routes.MAIN]: MainScreen,
});

export default createAppContainer(MainNavigator);
