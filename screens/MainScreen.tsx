import React from "react";
import { StyleSheet, Text, View } from "react-native";
import { NavigationStackScreenComponent } from "react-navigation-stack";

interface MainScreenNavigationParams {}

interface MainScreenProps {}

const MainScreen: NavigationStackScreenComponent<
  MainScreenNavigationParams,
  MainScreenProps
> = (props) => {
  return (
    <View>
      <Text>Hello</Text>
    </View>
  );
};

const styles = StyleSheet.create({});

export default MainScreen;
